# python-snippets

Just a repo for any piece of code not worthy of it's own repo, but still worthy to be archived, at least for a while

### pytest-veth-fixture

Small pytest fixture and example of how to use the fixture.
The fixture creates a pair of veths interfaces, up them and remove IPv6 addresses, before making them available to a 
test unit (our tested function then needs to send packets from one end on the veth and sniff packets from the other end). 
Removing IPv6 addresses is 'needed' (i.e. "I don't have a better workaround yet") because otherwise, we may end up with some automatic IPv6 packets in captures, and in this case we only care about IPv4.
