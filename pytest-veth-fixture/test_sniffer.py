""" testing module sniffer:
"""

import os
import sys
import logging
from time import sleep

import pytest
from pyroute2 import IPRoute, NetlinkError
from pyroute2.iproute.linux import AF_INET6
from scapy.all import raw

from sniffer import run


@pytest.fixture(scope="module")
def veths_setup():
    """ Fixture for creating a veth pair, up both interfaces and removing their
        IPv6 address (thus avoiding 'parasite' packets in captures)
    """

    log = logging.getLogger()
    veth_names = ("vTest1", "vTest2")

    # Setup
    with IPRoute() as ipr:
        try:
            # Create and up interfaces
            ret = ipr.link("add", ifname=veth_names[0], kind="veth", peer=veth_names[1])
            log.debug(f"Interface creation : {ret}")
            ipr.link("set", ifname=veth_names[0], state="up")
            ipr.link("set", ifname=veth_names[1], state="up")

            # Find newly created veths and remove their IPv6 addr
            for veth in veth_names:
                addresses = None
                while not addresses:  # active wait before addresses actually appear on interface
                    veth_idx = ipr.link_lookup(ifname=veth)[0]
                    addresses = ipr.get_addr(index=veth_idx, family=AF_INET6)
                    log.debug(f"Addresses for interace {veth} : {addresses}")
                    for addr in addresses:
                        ipr.addr(
                            "del",
                            index=veth_idx,
                            address=addr.get_attr("IFA_ADDRESS"),
                            mask=addr["prefixlen"],
                        )

        except NetlinkError as err:
            log.error(f"pyroute2 error : {err}")
            sys.exit(1)
        except IndexError as err:
            log.error(f"IndexError : {err}")
            sys.exit(1)

        sleep(1)  # There's always a few packets emitted right after upping iface

        yield veth_names

        # Teardown
        try:
            ret = ipr.link("del", ifname=veth_names[0])
        except NetlinkError as err:
            log.error("Unable to delete veth")


def test_sniffer(veths_setup):
    """ Basic test of sniffer module """

    veth_send = veths_setup[0]
    veth_sniff = veths_setup[1]

    payload, sniffed = run(veth_send, veth_sniff)

    assert len(payload) == len(sniffed)
    for payload_pkt, sniffed_pkt in zip(payload, sniffed):
        assert raw(payload_pkt) == raw(sniffed_pkt)
