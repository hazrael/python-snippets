#!/usr/bin/env python3


""" Small Scapy test that sends packets to a given veth endpoint while sniffing on the
    peer.

    Dummy code used during pytest fixture development ('test_sniffer.py')

"""

from scapy.layers.l2 import Ether
from scapy.layers.inet import IP, TCP
from scapy.sendrecv import sendp, AsyncSniffer

from time import sleep


def generate_packets():
    """ Dummy packet generation """

    return [
        Ether() / IP() / TCP() / "Packet1",
        Ether() / IP() / TCP() / "Packet2",
        Ether() / IP() / TCP() / "Packet3",
        Ether() / IP() / TCP() / "Packet4",
    ] * 256


def send(pkts, if_send, if_sniff):
    """ Send packets using one end of a veth pair and sniffing on the other end

        :param list pkts Packets to be sent
        :param string if_send Name of veth interface used for sending packets
        :param string if_sniff name of veth interface used for sniffinf packets
        :return Packets sniffed on `if_sniff`
        :rtype: list
    """

    asnf = AsyncSniffer(iface=if_sniff, count=0, store=True)
    asnf.start()
    sleep(1)  # Give time to the sniffer for startup

    sendp(pkts, iface=if_send)

    sleep(1)  # Make sure all packets have been sniffed before stopping
    results = asnf.stop()

    return results


def run(if_send, if_sniff):
    """ Run and return both sent packets and sniffed packets

        :param string if_send Name of veth interface used for sending packets
        :param string if_sniff name of veth interface used for sniffinf packets
        :return Tuple of generated packets and sniffed packets
        :rtype: tuple
    """

    gen_pkts = generate_packets()

    sniffed_pkts = send(gen_pkts, if_send, if_sniff)

    return (gen_pkts, sniffed_pkts)


if __name__ == "__main__":
    run()
